﻿var express = require('express');
var router = express.Router();
var crypto = require('crypto'); 
var jwt = require('jsonwebtoken');
var session = require('express-session');
var utils = require('./utils');
var auth = require('./auth');
var dataContext = require('./dataContext');

router.get('/api/service/method', function (req, res) {
    utils.isAccessAllowed(req, function (isAllowed) {
        if (false && !isAllowed) {
            res.json(401, { msg: "Access denied." })
        }
        else {
            res.json(200, { msg: "Access allowed." })
        }
    });
});

router.post('/api/users/login', function (req, res) {
    auth.login(req, function (err, data) {

    });
    res.json(200, { msg: "logged in" });
});
router.post('/api/users/logout', function (req, res) {
    auth.logout(req, function (err, data) {

    });
    res.json(200, { msg: "logged out" });
});
router.post('/api/users/resetpassword', function (req, res) {
    auth.resetPassword(req, function (err, data) {

    });
    res.json(200, { msg: "password reset" });
});
router.post('/api/users/create', function (req, res) {
    dataContext.createUser(req, function (err, data) {

    });
    res.json(202, { msg: "created", data: { } });
});

router.get('/api/users/get', function (req, res) {
    utils.isAccessAllowed(req, function (isAllowed) {
        
        if (false && !isAllowed) {
            res.status(401).json({ msg: "Hello world" });
        }
        else {
            dataContext.getUser(req, function (err, data) {
                res.status(200).json({ msg: "Goodbye world. object id: " + data.id + ", name: " + data.name });
            });
        }
    });
});
router.post('/api/users/update', function (req, res) {
    dataContext.updateUser(req, function (err, data) {

    });
    res.json(202, { msg: "updated", data: {  } });
});
router.post('/api/users/delete', function (req, res) {
    dataContext.deleteUser(req, function (err, data) {

    });
    res.json(202, { msg: "deleted", data: { } });
});

module.exports = router;