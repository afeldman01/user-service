﻿var crypto = require('crypto');
var jwt = require('jsonwebtoken');

module.exports = {

    isAccessAllowed: function (req, callback) {
        var token = typeof (req.headers['auth-token']) != "undefined" ? req.headers['auth-token'] : typeof (req.query.token) != "undefined" ? req.query.token : typeof (req.body) != "undefined" ? req.body.token : "";

        if (token) {
            jwt.verify(token, req.app.get('superSecret'), function (err, decoded) {
                if (err) {
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }
        else {
            var token = req.session.token;

            if (!token) {
                callback(false);
            }
            else {
                jwt.verify(token, req.app.get('superSecret'), function (err, decoded) {
                    if (err) {
                        callback(false);
                    } else {
                        callback(true);
                    }
                });
            }
        }
    }, 

    hashPassword: function (password, salt) {
        var sum = crypto.createHash('sha256');
        sum.update(password + salt);
        return sum.digest('base64');
    },

    genuuid: function (callback) {
        if (typeof (callback) !== 'function') {
            return this.uuidFromBytes(crypto.randomBytes(16));
        }

        crypto.randomBytes(16, function (err, rnd) {
            if (err) return callback(err);
            callback(null, this.uuidFromBytes(rnd));
        });
    },

    uuidFromBytes: function (rnd) {
        rnd[6] = (rnd[6] & 0x0f) | 0x40;
        rnd[8] = (rnd[8] & 0x3f) | 0x80;
        rnd = rnd.toString('hex').match(/(.{8})(.{4})(.{4})(.{4})(.{12})/);
        rnd.shift();
        return rnd.join('-');
    },

    getToken: function (req) {
        return typeof (req.headers['auth-token']) != "undefined" ? req.headers['auth-token'] : typeof (req.query.token) != "undefined" ? req.query.token : typeof (req.body) != "undefined" ? req.body.token : "";
    },
    randomString: function (length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    },
    isValidJson: function (text) {
        try {
            JSON.parse(text);
            return true;
        }
        catch (ex) {
            return false;
        }
    },

};