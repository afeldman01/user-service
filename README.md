# README #

https://www.fbo.gov/downloads/fbo_web_services_technical_documentation.pdf

### What is this repository for? ###

* Scape odds, scores and other sports gambling data
* Alpha


### How do I get set up? ###

* Clone the Repo
https://bitbucket.org/afeldman01/odds_service.git
* Install node and npm
https://nodejs.org/en/download/
* Install visual studio with node js extension 
https://www.visualstudio.com/vs/node-js/
* Go to the directory in the terminal and download the dependencies via npm
npm Install
* Open the app in Visual studio. Click debug to run the application
* OR run from the command line via 
node app.js


### Contribution guidelines ###

* Please write unit tests for all core functionality
* Please write clean code that is easy to read
* If you add any dependencies that need to be noted update the setup instructions
* Update package.json with all new dependencies you add. 

### Contribution ###

* stiphel 