﻿var express = require('express'),
    app = express();
var os = require('os'); 
var bodyParser = require('body-parser');
var utils = require('./routes/utils');

var devPcs = [
    "Adam-PC",
    "DESKTOP-4KGTIPP",
    "DESKTOP-9Q6Q2LS"
];

app.locals.tempPaths = [
    "i:\\tmp\\familybox\\uploads",
    "/tmp/familybox/uploads"
];

app.locals.sperator = "/";

 
app.locals.env = devPcs.indexOf(os.hostname()) > -1 ? 0 : 1;
app.locals.databaseServer = ["http://13.93.210.214:8080/databases/FMC", "http://10.0.0.10:8080/databases/FMC", "http://13.93.210.214:8080/databases/FMC", "http://192.168.11.37:8080/databases/FMC"]; 
app.locals.temp = app.locals.tempPaths[app.locals.env];
if (app.locals.env == 0) {
    app.locals.sperator = "\\"; 
}
 
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json()); 
app.use(bodyParser.json({ type: 'application/*+json' }))

app.use(function (req, res, next) { 
    res.setHeader('Access-Control-Allow-Origin', '*'); 
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); 
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, auth-token, Boundary'); 
    res.setHeader('Access-Control-Allow-Credentials', true); 
    next();
});
 

var session = require('express-session');

// https://github.com/expressjs/session
var sess = {
    secret: ',a,S^6@q_!J=GyBf',
    cookie: {},
    genid: function (req) {
        return utils.genuuid()  
    },
    saveUninitialized: false,
    resave: false
}

app.use(session(sess))

app.set('superSecret', ")ct<vxYE72U+/Mb'");

var routes = require('./routes/route');
app.use('/', routes); 

module.exports = app;

var server = app.listen(84, function () {
    var port = server.address().port;
    console.log('Express server listening on port %s.', port);
});

 